# Doer

Dead-simple taskbar ticker display of your immediate todo items.

## Setup

1. Install [xbar](https://xbarapp.com/) if you haven't already
2. Add the Doer plugin
3. Create a list in Reminders name "Doer" and add your items
