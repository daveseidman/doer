#!/usr/bin/env bash

# <xbar.title>Reminders</xbar.title>
# <xbar.version>v1.0</xbar.version>
# <xbar.author>Dave Seidman</xbar.author>
# <xbar.author.github>DaveSeidman</xbar.author.github>
# <xbar.desc>Create a list in reminders named "Doer" and they'll display in this taskbar ticker.</xbar.desc>
# <xbar.dependencies>bash, osascript</xbar.dependencies>
# <xbar.abouturl>https://gitlab.com/daveseidman/doer</xbar.abouturl>
# <xbar.var>number(VAR_MAX=5): Max Dos</xbar.var>

export PATH="$PATH:/usr/local/bin"

reminders=$(osascript -e '
  tell application "Reminders" 
    return (name of every reminder in list "Doer" whose completed is false)
  end tell
')

IFS=$',' read -ra reminder_array <<< "$reminders"

count=0
for reminder in "${reminder_array[@]}"; do
  if [ "$count" -ge "$VAR_MAX" ]; then
    break
  fi

  echo "☑ $reminder"
  count=$((count + 1))
  sleep 1
done

awk '/^Host / && !/\*/ {print $2" | shell=ssh param1="$2 " terminal=true"}' ~/.ssh/config